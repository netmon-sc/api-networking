<?php

namespace Netmon\Networking\Serializers;

use Gate;

use ApiServer\JsonApi\Serializers\BaseSerializer;

use Netmon\Networking\Models\Ip;

class IpSerializer extends BaseSerializer
{
    protected $type = 'ips';

    public function getAttributes($model, array $fields = null)
    {
        if (! ($model instanceof Ip)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.Ip::class
            );
        }

        // set up attributes
        $attributes = $model->getAttributes();
        $attributes['created_at']  = $this->formatDate($model->created_at);
        $attributes['updated_at'] = $this->formatDate($model->updated_at);

        return $attributes;
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/ips/{$model->id}",
        ];

        //links to include based permissions
        if(Gate::allows('show', $model))
            $links['read'] = config('app.url')."/ips/{$model->id}";
        if(Gate::allows('update', $model))
            $links['update'] = config('app.url')."/ips/{$model->id}";
        if(Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/ips/{$model->id}";

        return $links;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function networkInterface($model)
    {
        return $this->belongsTo($model, \Netmon\Interfaces\Serializers\NetworkInterfaceSerializer::class);
    }
}

?>
