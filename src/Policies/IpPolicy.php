<?php

namespace Netmon\Networking\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use ApiServer\Authorization\Policies\BasePolicy;
use Netmon\Networking\Models\Ip;
use ApiServer\Users\Models\User;

class IpPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function index(User $authUser) {
        //everyone is allowed to list ips
        return true;
    }

    public function store(User $authUser, Ip $ip = null) {
        //if authenticated user is allowed to update the coresponding
        //networkInterface, then he is also allowed to store a new ip
        try {
            if(!is_null($ip)) {
                if(\Gate::allows('update', $ip->networkInterface)) {
                    return true;
                }
            }
        } catch(\Exception $e) {}

        return $this->checkPermissions($authUser, 'store', 'ip');
    }

    public function show(User $authUser, Ip $ip) {
        //everyone is allowed to show ip
        return true;
    }

    public function update(User $authUser, Ip $ip) {
        if(\Gate::allows('update', $ip->networkInterface)) {
            return true;
        }

        return $this->checkPermissions($authUser, 'update', 'ip', $ip);
    }

    public function destroy(User $authUser, Ip $ip) {
        if(\Gate::allows('destroy', $ip->networkInterface)) {
            return true;
        }

        return $this->checkPermissions($authUser, 'destroy', 'ip', $ip);
    }
}
