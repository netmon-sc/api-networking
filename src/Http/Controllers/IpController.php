<?php

namespace Netmon\Networking\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class IpsController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Networking\Models\Ip::class;
    }

    public function serializer() {
        return \Netmon\Networking\Serializers\IpSerializer::class;
    }

    public function resource() {
        return "ips";
    }
}
