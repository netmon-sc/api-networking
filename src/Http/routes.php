<?php

Route::group([
    'namespace' => '\Netmon\Networking\Http\Controllers',
    'middleware' => 'cors'], function()
{
	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
        Route::resource('ips', IpsController::class, ['only' => [
			    'index', 'show', 'store', 'update', 'destroy'
		]]);
	});
});

?>
