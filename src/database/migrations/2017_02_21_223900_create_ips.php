<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ips', function (Blueprint $table) {
        	//meta
            $table->uuid('id')->primary();
            $table->uuid('network_interface_id');
            $table->foreign('network_interface_id')
	            ->references('id')
    	        ->on('network_interfaces')
        	    ->onDelete('cascade');

            //general
            $table->string('proto', 20);
            $table->string('family', 10);
            $table->string('address', 50)->nullable();
            $table->integer('mask')->nullable();
            $table->integer('gateway')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ips');
    }
}
