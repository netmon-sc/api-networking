<?php

namespace Netmon\Networking\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class RelationsServiceProvider extends ServiceProvider
{
    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        add_model_relation(
            'Netmon\Interfaces\Models\NetworkInterface',
            'ips',
            function($model) {
                return $model->hasMany('Netmon\Networking\Models\Ip');
            }
        );

        add_serializer_relation(
            'Netmon\Interfaces\Serializers\NetworkInterfaceSerializer',
            'ips',
            function($serializer, $model) {
                return $serializer->hasMany($model, 'Netmon\Networking\Serializers\IpSerializer');
            }
        );
    }
}
