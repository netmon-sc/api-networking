<?php

namespace Netmon\Networking\Models;

use ApiServer\Base\Models\BaseModel;
use ApiServer\Base\Traits\UuidForKeyTrait;

class Ip extends BaseModel
{
    use UuidForKeyTrait;

    /**
     * Bootstrap any application services.
     */
    public static function boot()
    {
        parent::boot();

        //Register validation service
        //on saving event
        self::saving(
            function ($model) {
                return $model->validate();
            }
        );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ips';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // meta
		'network_interface_id',

        // general
		'proto',
        'family',
        'address',
        'mask',
        'gateway',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
        // These attributes are inspired by the Ip object of
        // the NetJSON specification http://netjson.org/rfc.html#rfc.section.5.4

        // meta
        'network_interface_id' => 'required|exists:network_interfaces,id',

        // general
        'proto' => 'required|in:dhcp,static',
		'family' => 'required|in:ipv4,ipv6',
        'address' => 'required_if:proto,static|nullable|ip',
        'mask' => 'required_if:proto,static|nullable|integer',
        'gateway' => 'nullable|ip',
    ];

    /**
     * n:1 relation to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function networkInterface() {
    	return $this->belongsTo(\Netmon\Interfaces\Models\NetworkInterface::class);
    }
}
