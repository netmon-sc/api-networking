<?php

namespace Netmon\Networking\Tests;

use Netmon\Server\Tests\ModuleTestCase;
use Netmon\Server\Tests\UserTest;
use Netmon\Devices\Tests\DeviceTest;
use Netmon\Interfaces\Tests\NetworkInterfaceTest;

class IpTest extends ModuleTestCase
{
    protected function moduleServiceProvider() {
        return \Netmon\Networking\ModuleServiceProvider::class;
    }

    protected function moduleServiceProviderDepencies() {
        return [
            \Netmon\Devices\ModuleServiceProvider::class,
            \Netmon\Interfaces\ModuleServiceProvider::class,
        ];
    }

    public function getStructure() {
		return [
			'type',
			'id',
			'attributes' => [
				'proto',
                'family',
			]
		];
	}

    public function testUserCanCreateIp() {
		$user = UserTest::createUser();
		$device = DeviceTest::createDevice([
            'creator_id' => $user->id
        ]);
        $networkInterface = NetworkInterfaceTest::createNetworkInterface([
            'device_id' => $device->id
        ]);

        $authToken = \JWTAuth::fromUser($user);
		$headers = ['Authorization' => "Bearer {$authToken}"];

		$this->json(
			'POST',
			'/ips',
			[
				'data' => [
					'type' => "ip",
					'attributes' => [
                        'network_interface_id' => $networkInterface->id,
						'proto' => "static",
						'family' => "ipv6",
						'address' => "fe80::c094:70fe:24cf:a7a3",
                        'mask' => "64"
					]
				]
			],
			$headers
		);
		$this->assertResponseStatus(201);
		$this->seeJsonStructure($this->getResourceStrucure());
    }
}
?>
