# Networking Module for the Netmon API-Server
This project holds the Networking module for the Netmon API-Server. It handles layer 3 networks (IPv6 networks and IPv6 ip adresses).

## Installation
This module is automatically downloaded on the installation of the Netmon server
so you dont need to require it manually using `composer require`.

Just install and configure the module from within the root folder of
your Netmon Server installation by executing the following commands:
```
php artisan module:install Networking "Netmon\Networking\ModuleServiceProvider"
php artisan module:configure Networking
```

## Documentation
 * [Technology stack](doc/technology.md)
 * [Unittesting](doc/unittesting.md)
 * [Creating a release](doc/release.md)

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

## Mailinglist
https://lists.ffnw.de/mailman/listinfo/netmon-dev

# License
See [License](LICENSE.txt)
