## Unittesting
From within a working Netmon server installation run:
```
cd vendor/netmon-server/networking
phpunit --bootstrap ../../../bootstrap/autoload.php
```
